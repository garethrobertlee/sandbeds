const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const reviewSchema = new mongoose.Schema({
    name: String,
    author: {
      type: mongoose.Schema.ObjectId,
      ref: 'User'
    },
    review: {
      type: String,
      trim: true
    },
    rating:{
      type:Number,
      min:1,
      max:5},
    created: {
      type:Date,
      default:Date.now
    },
    store: 
        {
            type: mongoose.Schema.ObjectId,
            ref: 'Store'
        }
    
})

function autopopulate(next){
  this.populate('author');
  next();
}

reviewSchema.pre('find', autopopulate)
reviewSchema.pre('findOne', autopopulate)


module.exports = mongoose.model('Review', reviewSchema);