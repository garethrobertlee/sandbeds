const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const slug = require('slugs');

const storeSchema = new mongoose.Schema({
  name: {
      type: String,
      trim: true,
      required: 'please enter a store name'
  },
  slug: String,
  description: {
      type: String,
      trim: true
  },
  tags:[String],
  created: {
      type:Date,
      default:Date.now
  },
  location: {
      type: {
          type: String,
          default: 'Point'
      },
      coordinates: [{
          type: Number,
          required: "You must supply coords"
      }],
      address: {
          type: String,
          required: "you must supply an address"
      }
  },
  photo: String,
  author: {
      type: mongoose.Schema.ObjectId,
      ref: 'User'
  }
})

storeSchema.index({
    name:'text',
    description: 'text',

})

storeSchema.index({
    reviews: 'count'

})

storeSchema.index({
    location: '2dsphere'
    
})

//todo - sanitize on save

storeSchema.pre('save', async function(next){
    if (!this.isModified('name')){
        next();
        return;
    }
    this.slug = slug(this.name);
    const slugRegEx = new RegExp(`^(${this.slug})((-[0-9]*$)?)$`, 'i');
    const storesWithSlug = await this.constructor.find({slug:slugRegEx})
    if (storesWithSlug.length){
        this.slug = `${this.slug}-${storesWithSlug.length + 1}`;
    }
    next();
})//TODO make slugs uniqque

storeSchema.statics.getTagsList = function(){
    return this.aggregate([
        { $unwind: '$tags'},
        { $group: {_id: '$tags', count: {$sum: 1}}},
        { $sort: { count: -1 }}
    ])
}

storeSchema.statics.reviewCount = function(){
    return this.aggregate([
        //lookup
        {$lookup: {from: 'reviews', localField: '_id', foreignField: 'store', as: 'reviews'}},
        //match where there is one review
        {$match: { 'reviews.1': {$exists: true}}},
        // create new field
        { $project: {
            photo: '$$ROOT.photo',
            name: '$$ROOT.name',
            reviews: '$$ROOT.reviews',
            slug: '$$ROOT.slug',
            averageRating: { $avg: '$reviews.rating'}
        }},
        { $sort: { averageRating: -1 }},
        { $limit: 10}
    ])
}


storeSchema.virtual('reviews', {
    ref: 'Review',
    localField: '_id',
    foreignField: 'store'
})

function autopopulate(next){
  this.populate('review');
  next();
}

storeSchema.pre('find', autopopulate)

// storeSchema.options = {
//     toObject: {
//       virtuals: true
//     }
//     ,toJSON: {
//       virtuals: true
//     }
//   };



module.exports = mongoose.model('Store', storeSchema);
 