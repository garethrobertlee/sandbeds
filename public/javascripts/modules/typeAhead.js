import axios from 'axios';
import dompurify from 'dompurify'

function searchResultsHTML(stores){
    return stores.map(store => {
        return `<a href="/stores/${store.slug}" class="search__result"><strong>${store.name}</strong></a>`
    }).join('')
}

function typeAhead (search) {
  if (!search) return;

  const searchInput = search.querySelector('input[name="search"]')
  const searchResults = search.querySelector('.search__results')
  console.log(searchInput,searchResults)

  searchInput.on('input', function(){
      console.log(this.value)
      if (!this.value){
          searchResults.style.display = 'none';
          return;
      }
      searchResults.style.display = 'block';
    //   searchResults.innerHTML = '';
      axios
        .get(`/api/v1/search?q=${this.value}`)
        .then(res => {
            console.log(res)
            if(res.data.length){
                console.log('there is oething show')
                searchResults.innerHTML = dompurify.sanitize(searchResultsHTML(res.data));
                return
            }
            searchResults.innerHTML = dompurify.sanitize(`<div class="search__result">nothing 
              found for ${this.value}</div>`)
        })
        .catch(error => {
            console.log(error)
        })
  })


  searchInput.on('keyup', (e) => {
      if (![38,40,13].includes(e.keyCode)) return
      console.log(searchResults.innerHTML.split())
      const activeClass = 'search__result--active';
      const current = search.querySelector(`.${activeClass}`)
      const items = search.querySelectorAll('.search__result')
      let next
      if (e.keyCode === 40 && current){
          next = current.nextElementSibling || items[0]
      } else if (e.keyCode === 40){
          next = items[0]
      } else if (e.keyCode === 38 && current){
          next = current.previousElementSibling || items[items.length-1]
      } else if (e.keyCode ===38){
          next = items[items.length-1]
      } else if (e.keyCode === 13 && current){
          window.location = current.href;
      }
      if (current){
          current.classList.remove(activeClass)
      }
      next.classList.add(activeClass)
  })
}

export default typeAhead