import axios from 'axios';

const mapOptions = {
    center: {lat:53.7, lng:-1.94},
    zoom: 2
}


function loadPlaces(map,lat = 53.7, lng = -1.94){
    console.log(map)
    axios.get(`/api/v1/stores/near?lat=${lat}&lng=${lng}`)
    .then(res => {
        const places = res.data;
        console.log(places)
        if (!places.length){
            alert("nothing ound")
            return
        }

        const bounds = new google.maps.LatLngBounds();
        const infoWindow = new google.maps.InfoWindow();
        const markers = places.map(place => {
            const [placeLng, placeLat] = place.location.coordinates;
            console.log(placeLng,placeLat)
            const position = { lat: placeLat, lng: placeLng};
            bounds.extend(position)
            const marker = new google.maps.Marker({
                map,
                position
            });
            marker.place = place;
            return marker;
        })
        markers.forEach(marker => marker.addListener('click', function(){
            const html = `<div class="popup">
                           <a href="/stores/${this.place.slug}">
                           <img src="/uploads/${this.place.photo || 'store.png'}"/>
                           <span>${this.place.name}</span>
                           </a></div>`
            infoWindow.setContent(html);
            infoWindow.open(map, this)
        }))
     
        map.setCenter(bounds.getCenter());
        map.fitBounds(bounds)
    })

}


function makeMap(mapDiv){
    if (!mapDiv) return
    console.log(mapDiv)
    
    const map = new google.maps.Map(mapDiv,mapOptions)
    loadPlaces(map)
    const input = document.querySelector('[name="geolocate"]')
    const autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.addListener('place_changed', () => {
        const place = autocomplete.getPlace();
        console.log(place)
        loadPlaces(map,place.geometry.location.lat(),place.geometry.location.lng())
    })
}

export default makeMap;