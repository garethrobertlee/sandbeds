const mongoose = require('mongoose');
const User = mongoose.model('User');
const promisify = require('es6-promisify');



exports.loginForm = (req,res) => {
    res.render('login')
}

exports.registerForm = (req,res) => {
    res.render('register', {title:'register'})
}

exports.account = (req,res) => {
    res.render('account', {title:'edit your account'})
}

exports.updateAccount = async (req,res) => {
    console.log("es")
    const updates = {
        name: req.body.name,
        email: req.body.email
    }
    const user = await User.findOneAndUpdate(
        { _id: req.user._id },
        { $set: updates},
        {new: true, runValidators: true, context: 'query'}
    )
    req.flash('success',"changed")
    res.redirect('back')
}

exports.validateRegistration = (req,res,next) => {
//    res.send("what")
  req.sanitizeBody('name')
  req.checkBody('name', "you must supply a name").notEmpty();
  req.checkBody('email', "you mu8st suply n emil").isEmail();
  req.sanitizeBody('email').normalizeEmail({
      remove_dots:false,
      remove_extension: false,
      gmail_remove_subaddress:false
  });
  req.checkBody('password', "blankc passlwod!").notEmpty();
  req.checkBody('password-confirm', "blankc passlwod!").notEmpty();
  req.checkBody('password-confirm', "dont mach!").equals(req.body.password);


  const errors = req.validationErrors();
  if (errors){
    req.flash('error', errors.map(err => err.msg))
    res.render('register', {title: 'Register', body: req.body, flashes: req.flash()} )
    return;
  }
  next();
}

exports.register = async (req,res,next) => {
  const user = new User({
      email:req.body.email,
      name: req.body.name,
      
  });
//   User.register(user, req.body.password, function(err,user){}) //callback based
const registerWithPromise = promisify(User.register, User);
await registerWithPromise(user,req.body.password);
next();
}