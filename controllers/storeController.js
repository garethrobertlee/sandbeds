const mongoose = require('mongoose');
const Store = mongoose.model('Store');
const User = mongoose.model('User');
const Review = mongoose.model('Review');
const multer = require('multer');
const jimp = require('jimp');
const uuid = require('uuid');

const multerOptions = {
    storage: multer.memoryStorage(),
    fileFilter(req,file,next){
        const isPhoto = file.mimetype.startsWith('image/');
        if (isPhoto) {
            console.log("its a photo alright")
            next(null,true)
        } else {
            next({message:"that file isnt an image"}, false);
        }
    }
};



exports.homePage = (req,res) => {
    console.log(req.name)
    req.flash('error', "problem");
    res.render('index')
};

exports.addStore = (req,res) => {
    res.render('editStore', {
        title: 'Add Store'
    });
};

exports.viewStore = async (req,res,next) => {
    const store = await Store.findOne({slug:req.params.slug}).populate('author reviews eggs')

    if (!store) return next()
    res.render('store', {store, title: store.name, reviews: store.reviews})
};

// exports.viewStore = async (req,res,next) => {
//     const storePromise = Store.findOne({slug:req.params.slug}).populate('author reviews eggs')
//     const reviewsPromise = Review.find({slug:req.params.slug})
//     //  res.json(req.par/ams.id)
//     const [store,reviews] = await Promise.all([storePromise, reviewsPromise]);
//       console.log(store)
//       res.json(req.params)
//     //   res.json(reviews)
//     if (!store) return next()
//     res.render('store', {store, title: store.name, reviews})
// };

exports.upload = multer(multerOptions).single('photo');

exports.resize = async (req, res, next) => {
    console.log(req)
    console.log("barnsley")
    if (!req.file){
      next();
      return;
    }
    const extension = req.file.mimetype.split('/')[1];
    req.body.photo = `${uuid.v4()}.${extension}`; //unique
    const photo = await jimp.read(req.file.buffer);
    await photo.resize(800, jimp.AUTO);
    await photo.write(`./public/uploads/${req.body.photo}`);
    next();
}

exports.updateStore = async (req,res) => {
    req.body.location.type = "Point";
    const store = await Store.findOneAndUpdate({_id:req.params.id},req.body, {
        new: true,
        runValidators: true
    }).exec()
      req.flash('success', `succesfully created ${store.name}`);
    res.redirect(`/stores/${store._id}/edit`)
};

exports.createStore = async (req,res) => {
    req.body.author = req.user._id;
    const store = await (new Store(req.body)).save();
    console.log("saved!");
    req.flash('success', `succesfully created ${store.name}`);
    res.redirect(`/stores/${store.slug}`)
    // res.json(req.body)
}

exports.topStores = async (req,res) => {
    const stores = await Store.reviewCount()
     res.render('topStores', { title: "The Stores",stores})
}

exports.getStores = async (req,res) => {
    // res.json(req.user)
    const page = req.params.page || 1;
    const limit = 4;
    const skip = (page * limit) - limit;
    const countPromise = Store.count()
    const storesPromise = Store
    .find()
    .skip(skip)
    .limit(limit)
    .sort({created: 'desc'})
    // res.json(stores)
    const [count,stores] = await Promise.all([countPromise, storesPromise]);
    const pages = Math.ceil(count / 4)
    if (!stores.length && skip){
        req.flash('warning',"no more stores")
        res.redirect(`/stores/page/${pages}`)
        return
    }
    res.render('stores', { title: "The Stores", page, stores, count, pages})
}

exports.getStoresByTag = async (req,res) => {
    const tag = req.params.tag
    const tagQuery = tag || { $exists:true};
    const tagsPromise = Store.getTagsList()
    const storesPromise = Store.find({tags: tagQuery})   
    const [tags,stores] = await Promise.all([tagsPromise, storesPromise]);
    
    // res.json(tags)
    res.render('tags', { tags, tag, stores, title: 'tags'})
}

const confirmOwner = (store,user) => {
  if (!store.author.equals(user._id)){
      throw Error("not your store")
  }
}

exports.mapStores = async (req, res) => {
    const coords =[req.query.lng,req.query.lat].map(parseFloat);
    const q = {
        location: {
            $near: {
                $geometry: {
                    type: 'Point',
                    coordinates: coords
                },
                $maxDistance: 1000 //10km
            }
        }

    }

    const stores = await Store.find(q).select('photo name slug description location').limit(5)
    res.json(stores)
}

exports.mapPage = (req,res) => {
    res.render('map', {title:"map"})
}

exports.showHearts = async (req,res) => {
    // res.json(req.user.hearts)
    // res.json(req.user.hearts[0].toString())
    console.log(Store)
    const stores = req.user.hearts.map(heart => heart.toString())
    const heartedStores = await Store.find({_id: {$in: stores}})
    res.render('stores', { title: "favs", stores:heartedStores})
}



exports.heartStore = async (req, res) => {
   console.log(req.user._id)
   const hearts = req.user.hearts.map(obj => obj.toString())
   const operator = hearts.includes(req.params.id) ? '$pull' : '$addToSet';
   const user = await User.findByIdAndUpdate(
       req.user._id,
       { [operator]: { hearts: req.params.id}},
       {new:true}
    ) 
    console.log(user)
    res.json(user.hearts)
}

exports.searchStores = async (req,res) => {
    // res.json(req.query)
    const stores = await Store.find({
      $text: {
          $search: req.query.q
      }
    }, {
        score: {$meta:'textScore'}
    }).sort({
        score: {$meta: 'textScore'}
    }).limit(5)
    res.json(stores)
}

exports.editStore = async (req,res) => {

    const store = await Store.findOne({_id:req.params.id})
    confirmOwner(store, req.user)
    res.render('editStore', {title: `edit ${store.name}`, store})
    req.flash('error','not your store!')
    // res.render('stores', { title: "The Stores", stores})
}

