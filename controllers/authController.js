const passport = require('passport');
const crypto = require('crypto');
const mongoose = require('mongoose');
const User = mongoose.model('User');
const promisify = require('es6-promisify');
const mail = require('../handlers/mailHandler');

exports.login = passport.authenticate('local', {
    failureRedirect: '/login',
    failureFlash: 'Failed Login',
    successRedirect: "/",
    successFlash:"logged in!"
})

exports.logout = (req,res) => {
    req.logout()
    req.flash("success", "logged out!")
    res.redirect("/")
}

exports.isLoggedin = (req,res,next) => {
    if (req.isAuthenticated()){
        next();
        return 
    };
    req.flash("error","must be logged in");
    res.redirect("/login");

}

exports.forgot = async (req,res) => {
    const user = await User.findOne({email:req.body.email})
    if(!user){
        req.flash("error","no ecist")
        return res.redirect("/login");
    }
    user.resetPasswordToken = crypto.randomBytes(20).toString('hex');
    user.resetPasswordExpires = Date.now() + 3600000;
    await user.save();
    const resetURL = `http://${req.headers.host}/account/reset/${user.resetPasswordToken}`;
    req.flash('success', `checkyourmail}`)
    await mail.send({
        user,
        subject:'password reset',
        resetURL,
        filename: 'password-reset'
    })



    res.redirect("/login");
}

exports.confirmedPasswords = (req,res,next) => {
    if (req.body.password === req.body['password-confirm']){
        next()
        return;
    }
    req.flash("error",`${req.body.password} and ${req.body['password-confirm']}`)
    res.redirect('back')
}

exports.update = async (req, res) => {
    user = await User.findOne({
        resetPasswordToken: req.params.token,
        resetPasswordExpires: {$gt: Date.now()}
    });
      if (!user){
        req.flash('error','invalid token')
        res.redirect('/login')
    }
    const setPass = promisify(user.setPassword,user);
    await setPass(req.body.password)
    user.resetPasswordExpires = undefined
    user.resetPasswordToken = undefined
    const updated = await user.save()
    await req.login(updated)
    req.flash('success','password reset')
    res.redirect("/");
}

exports.reset = async (req,res) => {
    user = await User.findOne({
        resetPasswordToken: req.params.token,
        resetPasswordExpires: {$gt: Date.now()}
    })
    if (!user){
        req.flash('error','invalid token')
        res.redirect('/login')
    }
    res.render('reset',{title:'reset your password'})
}