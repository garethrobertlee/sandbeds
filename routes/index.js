const express = require('express');
const router = express.Router();
const storeController = require('../controllers/storeController');
const loginController = require('../controllers/loginController');
const authController = require('../controllers/authController');
const reviewController = require('../controllers/reviewController');

const { catchErrors } = require('../handlers/errorHandlers');

// Do work here
router.get('/', storeController.getStores);
router.get('/stores', storeController.getStores);
router.get('/stores/page/:page', storeController.getStores);

router.get('/stores/:id/edit', catchErrors(storeController.editStore));
router.get('/stores/:slug', catchErrors(storeController.viewStore));
router.get('/add', 
  authController.isLoggedin,
  storeController.addStore
  );
router.post('/add', 
  storeController.upload,
  catchErrors(storeController.resize),
  catchErrors(storeController.createStore)
);
router.post('/add/:id', 
  storeController.upload,
  catchErrors(storeController.resize),
  catchErrors(storeController.updateStore)
);

router.get('/hearts',  authController.isLoggedin,catchErrors(storeController.showHearts))
router.get('/api/v1/search', catchErrors(storeController.searchStores))
router.post('/api/v1/stores/:id/heart', catchErrors(storeController.heartStore));
router.post('/api/v1/stores/:id/review', catchErrors(reviewController.reviewStore));
router.get('/tags', catchErrors(storeController.getStoresByTag));
router.get('/tags/:tag', catchErrors(storeController.getStoresByTag));

router.get('/login', loginController.loginForm);
router.post('/login', authController.login);
router.get('/logout', authController.logout);
router.get('/register', loginController.registerForm);
router.post('/register', 
  loginController.validateRegistration,
  loginController.register,
  authController.login
  );

router.get('/account', 
  authController.isLoggedin,
  loginController.account
);

router.get('/top', storeController.topStores);
router.post('/account',   catchErrors(loginController.updateAccount));

router.post('/account/forgot', catchErrors(authController.forgot))

router.get('/account/reset/:token', catchErrors(authController.reset));
router.post('/account/reset/:token', 
  authController.confirmedPasswords,
  catchErrors(authController.update));

router.get('/map', storeController.mapPage)
router.get('/api/v1/stores/near', catchErrors(storeController.mapStores))

router.get('/reverse/:name', (req,res)=>{
  const rev = [...req.params.name].reverse().join('')
  res.send(rev)
})

module.exports = router;
